using Microsoft.AspNetCore.Mvc;
using TodoApi.API;
using System.Linq;
using TodoApi.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;

namespace TodoApi.Web.Controllers
{
    [Route("api/[controller]")]
    public class TodoController : Controller
    {
        private readonly ITodoItemService service;

        public TodoController(ITodoItemService service)
        {
            this.service = service;

            var items = service.FindAll();
            if (!items.Result.Any())
            {
                var newItem = new TodoItem()
                {
                    Name = "Dummy TODO item",
                    IsComplete = false
                };
                service.Create(newItem);
            }
        }

        [HttpGet]
        public async Task<IActionResult> FindAll()
        {
            var items = await service.FindAll();
            return Ok(items);
        }

        [HttpGet("{id}", Name = "GetTodo")]
        public async Task<IActionResult> FindById(long id)
        {
            var item = await service.FindById(id);
            if (item == null)
                return NotFound();
            return Ok(item);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody]TodoItem value)
        {
            var id = await service.Create(value);
            return CreatedAtRoute("GetTodo", new { id = id }, id);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(long id, [FromBody]TodoItem value)
        {
            // Make sure correct id is set before updating
            value.Id = id;
            await service.Update(value);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            await service.Delete(id);
            return NoContent();
        }
    }
}
