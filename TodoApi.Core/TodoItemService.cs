using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TodoApi.API;
using TodoApi.Core.Persistence;
using TodoApi.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace TodoApi.Core
{
    public class TodoItemService : ITodoItemService
    {
        private readonly TodoContext context;

        public TodoItemService(TodoContext context)
        {
            this.context = context;
        }

        public async Task<long> Create(TodoItem item)
        {
            var newItem = context.AddAsync(item);
            await context.SaveChangesAsync();

            await context.Entry(item).ReloadAsync();
            return item.Id;
        }

        public async Task Delete(long id)
        {
            var item = await FindById(id);
            if (item == null)
                throw new Exception($"Could not find item with id {id}");

            context.TodoItems.Remove(item);
            await context.SaveChangesAsync();
        }

        public async Task<IList<TodoItem>> FindAll()
        {
            return await context.TodoItems.ToAsyncEnumerable().ToList();
        }

        public async Task<TodoItem> FindById(long id)
        {
            return await context.TodoItems.FindAsync(id);
        }

        public async Task Update(TodoItem item)
        {
            var existingItem = await context.TodoItems.FirstOrDefaultAsync(i => i.Id == item.Id);
            if (existingItem == null)
                throw new Exception($"Could not find item with id {item.Id}");

            existingItem.Name = item.Name;
            existingItem.IsComplete = item.IsComplete;

            await context.SaveChangesAsync();
        }
    }
}
