using System.Collections.Generic;
using System.Threading.Tasks;
using TodoApi.Model;

namespace TodoApi.API
{
    public interface ITodoItemService
    {
        Task<IList<TodoItem>> FindAll();
        Task<TodoItem> FindById(long id);
        Task<long> Create(TodoItem item);
        Task Update(TodoItem item);
        Task Delete(long id);
    }
}
